package muic.backend.hw2.mypastebin.paste;

import muic.backend.hw2.mypastebin.model.Paste;
import muic.backend.hw2.mypastebin.repositories.PasteRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import static org.assertj.core.api.Assertions.assertThat;


import java.util.List;

@DataJpaTest
public class PasteRepositoryTest extends BaseTest{
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private PasteRepository pasteRepository;

    @Test
    public void whenFindById_thenReturnProduct() {

        Paste expectedPaste = getValidPaste();
        testEntityManager.persist(expectedPaste);

        Paste actualPaste = pasteRepository.findById(1L).get();

        assertThat(actualPaste.getTitle()).isEqualTo(expectedPaste.getTitle());
        assertThat(actualPaste.getContent()).isEqualTo(expectedPaste.getContent());
    }
    @Test
    public void whenFindAll_thenReturnPasteList() {
        Paste expectedPaste = getValidPaste();
        testEntityManager.persist(expectedPaste);


        List<Paste> pastes = pasteRepository.findAll();

        assertThat(pastes).hasSize(1);
    }

    @Test
    public void whenFindTop100_thenReturnPasteList() {
        Paste expectedPaste = getValidPaste();
        testEntityManager.persist(expectedPaste);


        List<Paste> pastes = pasteRepository.findTop100ByOrderByCreatedAtDesc();

        assertThat(pastes).hasSize(1);
    }



}
