package muic.backend.hw2.mypastebin.paste;


import muic.backend.hw2.mypastebin.model.Paste;
import muic.backend.hw2.mypastebin.model.dto.PasteDTO;
import muic.backend.hw2.mypastebin.model.dto.PasteIdDTO;
import muic.backend.hw2.mypastebin.repositories.PasteRepository;
import muic.backend.hw2.mypastebin.services.PasteServiceImpl;
import muic.backend.hw2.mypastebin.services.map.PasteMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class PasteServiceTest extends BaseTest{
    @Mock
    private PasteRepository pasteRepository;

    @InjectMocks
    private PasteServiceImpl pasteService;

    @Mock
    private PasteMapper pasteMapper;


    @Test
    public void whenFindAll_thenReturnPasteList(){

        PasteDTO pasteDTO = getValidPasteDTO();
        Paste paste = getValidPaste();
        List<Paste> pastes= Arrays.asList(paste);
        List<PasteDTO> expectedPasteDTOs = Arrays.asList(pasteDTO);

        doReturn(pastes).when(pasteRepository).findTop100ByOrderByCreatedAtDesc();
        doReturn(expectedPasteDTOs).when(pasteMapper).toPasteDTOs(any());

        List<PasteDTO> actualPaste = pasteService.findAll();

        assertThat(actualPaste).isEqualTo(expectedPasteDTOs);
    }

    @Test
    public void whenFindById_thenReturnPaste() {
        PasteDTO pasteDTO = getValidPasteDTO();
        Paste paste = getValidPaste();

        doReturn(Optional.of(paste)).when(pasteRepository).findById(anyLong());
        doReturn(pasteDTO).when(pasteMapper).toPasteDTO(any());

        PasteDTO actualPaste = pasteService.findById(anyLong());

        assertThat(actualPaste).isEqualTo(pasteDTO);
    }

    @Test
    public void whenSave_thenReturnPaste() {
        Paste paste = getValidPaste();
        PasteIdDTO pasteIdDTO = getValidPasteIdDTO();

        doReturn(paste).when(pasteRepository).save(paste);
        doReturn(pasteIdDTO).when(pasteMapper).toPasteIdDTO(any());

        PasteIdDTO actualPaste = pasteService.save(paste);

        assertThat(actualPaste).isEqualTo(pasteIdDTO);
    }

}
