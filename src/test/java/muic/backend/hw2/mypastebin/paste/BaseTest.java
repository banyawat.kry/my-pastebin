package muic.backend.hw2.mypastebin.paste;

import muic.backend.hw2.mypastebin.model.Paste;
import muic.backend.hw2.mypastebin.model.dto.PasteDTO;
import muic.backend.hw2.mypastebin.model.dto.PasteIdDTO;

public class BaseTest {

    Paste getValidPaste() {
        return Paste.builder()
                .title("Test tittle")
                .content("Test content")
                .build();

    }

    PasteDTO getValidPasteDTO() {
        return PasteDTO.builder()
                .title("Test title")
                .content("Test content")
                .build();

    }

    PasteIdDTO getValidPasteIdDTO() {
        return PasteIdDTO.builder()
                .id(1L)
                .build();

    }

}
