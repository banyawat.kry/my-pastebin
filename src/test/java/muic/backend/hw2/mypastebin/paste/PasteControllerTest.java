package muic.backend.hw2.mypastebin.paste;

import com.fasterxml.jackson.databind.ObjectMapper;
import muic.backend.hw2.mypastebin.controllers.PasteController;
import muic.backend.hw2.mypastebin.model.Paste;
import muic.backend.hw2.mypastebin.model.dto.PasteDTO;
import muic.backend.hw2.mypastebin.model.dto.PasteIdDTO;
import muic.backend.hw2.mypastebin.services.PasteService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import java.util.*;

@WebMvcTest(PasteController.class)
public class PasteControllerTest extends BaseTest{
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PasteService pasteService;

    @Autowired
    ObjectMapper objectMapper;


    @Test
    void whenFindById_thenReturnPasteDTO() throws Exception{

        PasteDTO pasteDTO = getValidPasteDTO();

        doReturn(pasteDTO).when(pasteService).findById(anyLong());

        mockMvc.perform(get("/api/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(pasteDTO.getTitle())))
                .andExpect(jsonPath("$.content", is(pasteDTO.getContent())));
    }

    @Test
    void whenFindAll_thenReturnPasteDTOList() throws Exception {

        PasteDTO pasteDTO = getValidPasteDTO();

        List<PasteDTO> pasteDTOs = Arrays.asList(pasteDTO);

        doReturn(pasteDTOs).when(pasteService).findAll();

        mockMvc.perform(get("/api/recents").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title", is(pasteDTO.getTitle())))
                .andExpect(jsonPath("$[0].content", is(pasteDTO.getContent())));
    }

    @Test
    void whenCreate_thenReturnPasteIdDTO() throws Exception {
        PasteDTO pasteDTO = getValidPasteDTO();

        PasteIdDTO pasteIdDTO = PasteIdDTO.builder()
                .id(1L)
                .build();

        String pasteDTOJson = objectMapper.writeValueAsString(pasteDTO);

        Paste paste = new Paste();
        paste.setId(1L);
        doReturn(pasteIdDTO).when(pasteService).save(any());

        mockMvc.perform(post("/api/paste")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pasteDTOJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(pasteIdDTO.getId().intValue())));
    }



}
