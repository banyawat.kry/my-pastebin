package muic.backend.hw2.mypastebin.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import muic.backend.hw2.mypastebin.model.Paste;
import muic.backend.hw2.mypastebin.model.dto.PasteDTO;
import muic.backend.hw2.mypastebin.model.dto.PasteIdDTO;
import muic.backend.hw2.mypastebin.services.PasteService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


@Slf4j
@Validated
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class PasteController {

    private final PasteService pasteService;

    @PostMapping("/paste")
    public ResponseEntity<PasteIdDTO> create(@Valid @NotNull @RequestBody Paste paste) {
        val pasteIdDTO = pasteService.save(paste);
        val headers = new HttpHeaders();

        headers.add("Location", "/api/" + pasteIdDTO.getId());

        return ResponseEntity.ok()
                .headers(headers)
                .body(pasteIdDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PasteDTO> findById(@PathVariable  @NotNull Long id) {
        return  ResponseEntity
                .ok(pasteService.findById(id));
    }

    @GetMapping("/recents")
    public ResponseEntity<List<PasteDTO>> findAll() {
        return ResponseEntity.ok(pasteService.findAll());
    }
}
