package muic.backend.hw2.mypastebin.repositories;

import muic.backend.hw2.mypastebin.model.Paste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PasteRepository extends JpaRepository<Paste, Long> {
    List<Paste> findTop100ByOrderByCreatedAtDesc();
}
