package muic.backend.hw2.mypastebin;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class MyPastebinApplication {

    public static void main(String[] args) {

        SpringApplication.run(MyPastebinApplication.class, args);

    }
}
