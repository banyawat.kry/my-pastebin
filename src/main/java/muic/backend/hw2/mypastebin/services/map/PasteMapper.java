package muic.backend.hw2.mypastebin.services.map;


import muic.backend.hw2.mypastebin.model.Paste;
import muic.backend.hw2.mypastebin.model.dto.PasteDTO;
import muic.backend.hw2.mypastebin.model.dto.PasteIdDTO;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper(uses = DateMapper.class)
public interface  PasteMapper {

   PasteDTO toPasteDTO(Paste paste);

   PasteIdDTO toPasteIdDTO(Paste paste);

   List<PasteDTO> toPasteDTOs(List<Paste> pastes);

   Paste toPaste(PasteDTO pasteDTO);
}
