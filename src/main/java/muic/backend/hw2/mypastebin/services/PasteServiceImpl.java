package muic.backend.hw2.mypastebin.services;


import lombok.RequiredArgsConstructor;
import muic.backend.hw2.mypastebin.exception.NotFoundException;
import muic.backend.hw2.mypastebin.model.Paste;
import muic.backend.hw2.mypastebin.model.dto.PasteDTO;
import muic.backend.hw2.mypastebin.model.dto.PasteIdDTO;
import muic.backend.hw2.mypastebin.repositories.PasteRepository;
import muic.backend.hw2.mypastebin.services.map.PasteMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class PasteServiceImpl implements PasteService{

    private final PasteRepository pasteRepository;
    private final PasteMapper pasteMapper;

    @Cacheable(value = "pastes", unless = "#result==null")
    @Override
    public List<PasteDTO> findAll() {
        return pasteMapper.toPasteDTOs(
                pasteRepository.findTop100ByOrderByCreatedAtDesc()
        );

    }

    @Cacheable(value = "paste", key = "#id", unless = "#result==null")
    @Override
    public PasteDTO findById(Long id) {
        return pasteMapper.toPasteDTO(
                pasteRepository.findById(id).orElseThrow(NotFoundException::new)
        );
    }


    @Caching(evict = {
            @CacheEvict(value="paste", key = "#paste.id"),
            @CacheEvict(value="pastes", allEntries=true) })
    @Override
    public PasteIdDTO save(Paste paste) {

        return pasteMapper.toPasteIdDTO(
                pasteRepository.save(paste)
        );

    }
}
