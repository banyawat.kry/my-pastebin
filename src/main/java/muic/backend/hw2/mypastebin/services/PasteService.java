package muic.backend.hw2.mypastebin.services;

import muic.backend.hw2.mypastebin.model.Paste;
import muic.backend.hw2.mypastebin.model.dto.PasteDTO;
import muic.backend.hw2.mypastebin.model.dto.PasteIdDTO;

import java.util.List;


public interface PasteService {

     List<PasteDTO> findAll();

     PasteDTO findById(Long id);

     PasteIdDTO save(Paste paste);
}
