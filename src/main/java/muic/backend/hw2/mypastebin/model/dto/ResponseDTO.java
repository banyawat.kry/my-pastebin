package muic.backend.hw2.mypastebin.model.dto;

import lombok.*;


@Data
@Builder
public class ResponseDTO{

    @Builder.Default
    private String error = "Success!";

}
