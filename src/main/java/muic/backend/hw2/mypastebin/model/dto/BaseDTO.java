package muic.backend.hw2.mypastebin.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;


import java.io.Serializable;
import java.time.OffsetDateTime;


@Data
public abstract class BaseDTO implements Serializable {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape=JsonFormat.Shape.STRING)
    private OffsetDateTime createdAt;
}