package muic.backend.hw2.mypastebin.model.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PasteIdDTO {
    private Long id;
}
