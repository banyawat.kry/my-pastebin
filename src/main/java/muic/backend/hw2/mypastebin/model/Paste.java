package muic.backend.hw2.mypastebin.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Entity
@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@EqualsAndHashCode(callSuper = true)
public class Paste extends BaseEntity{

    @NotBlank(message = "Title must not be blank")
    @Size(max = 512, message = "Title size must be between 0 and 512 characters")
    private String title;

    @NotBlank(message = "Content must not be blank")
    @Column(columnDefinition="TEXT")
    private String content;

}