package muic.backend.hw2.mypastebin.model.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PasteDTO extends BaseDTO {
    private String title;
    private String content;
}
